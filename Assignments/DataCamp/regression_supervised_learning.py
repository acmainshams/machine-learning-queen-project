import numpy as np

# Create X from the radio column's values
X = sales_df.iloc[:,1]

# Create y from the sales column's values
y = sales_df.iloc[:,3]


# print(X.shape)
# print(y.shape)
# # Reshape X
X = X.values.reshape(-1,1)

# # Check the shape of the features and targets
print(X.shape,y.shape)

########################## LinearRegression ########################################

# Import LinearRegression
from sklearn.linear_model import LinearRegression
# Create the model
reg = LinearRegression()

# Fit the model to the data
reg.fit(X,y)

# # Make predictions
predictions = reg.predict(X)
print(predictions[:5])
########################### matplotlib ########################################

# Import matplotlib.pyplot
import matplotlib.pyplot as plt

# Create scatter plot
plt.scatter(X, y, color="blue")

# Create line plot
plt.plot(X ,predictions,color="red")
plt.xlabel("Radio Expenditure ($)")
plt.ylabel("Sales ($)")

# Display the plot
plt.show()


